import { Subject } from "rxjs";
import { take } from "rxjs/operators";

export function wait(timeout: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, timeout));
}

export interface Deferred<T> extends Promise<T> {
  resolve(value: T): Promise<T>;
  reject(err?: any): Promise<any>;
}

export function newDeferred<T>(): Deferred<T> {
  const subject = new Subject<T>();
  const promise = subject.pipe(take(1)).toPromise();
  const resolve = (value: T) => {
    subject.next(value);
    return promise;
  };
  const reject = (err?: any) => {
    subject.error(err);
    return promise;
  };
  return Object.assign(promise, { resolve, reject });
}
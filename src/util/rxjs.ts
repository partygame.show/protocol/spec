import { OperatorFunction } from "rxjs";
import { scan } from "rxjs/operators";

export function collect<T>(): OperatorFunction<T, T[] | undefined> {
  return scan<T, T[]>((all, current) => [...all, current], []);
}

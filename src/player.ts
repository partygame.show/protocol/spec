import { LobbyCode, MessagePayload } from "./common/room";
import { Color, User } from "./common/user";
import { PlayerGameSession } from "./game";
import { Subscribable } from "./subscriber";

export interface PlayerMessage<REQUEST extends MessagePayload = MessagePayload, RESPONSE extends MessagePayload = MessagePayload> {
  readonly payload: REQUEST;
  reply(payload: RESPONSE): void;
  done(): Promise<void>;
}

export interface PlayerRoom {
  exit(): void;
  readonly closed: Promise<void>;
  readonly $games: Subscribable<PlayerGameSession>;
  readonly $messages: Subscribable<PlayerMessage<MessagePayload, MessagePayload>>;
  readonly $onClear: Subscribable<void>;
  readonly $state: Subscribable<object | null>;
}

export interface Player {
  readonly user: User;
  setDisplayName(value: string): Promise<void>;
  setColor(value: Color): Promise<void>;
  joinRoom(lobbyCode: LobbyCode): Promise<PlayerRoom>;
}

export interface PlayerApp {
  connect(displayName?: string, color?: string): Promise<Player>;
}


export const enum ErrorMessage {
  PackNotFound = "Specified pack was not found, may be disabled currently",
  GameNotFound = "Specified game was not found, may be disabled currently",
  BasePackUnowned = "Must own base pack for game to perform this operation",
  LobbyCodeNotFound = "Lobby code does not exist",
  UserKickedFromLobby = "User has been temporarily kicked from this lobby",
  UserBannedFromRoom = "User has been banned from this room",
  RoomUserLimitReached = "Cannot add another user, currently at limit for this room",
  GameAlreadyInProgress = "Cannot start new game when one is already in progress",
}

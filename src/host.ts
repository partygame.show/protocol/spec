import { Game, Gametype, PackID } from "./common/game";
import { LobbyCode, MessagePayload } from "./common/room";
import { User, UserID } from "./common/user";
import { HostGameSession } from "./game";
import { Subscribable } from "./subscriber";

export interface LobbyGuest {
  readonly user: User;
  add(): void;
  kick(): void;
  ban(): void;
}

export interface HostPlayer<PLAYER_STATE extends {} = object, REQUEST extends MessagePayload = MessagePayload, RESPONSE extends MessagePayload = MessagePayload> extends User {
  state: PLAYER_STATE | null;
  sendMessage(payload: REQUEST, expiration: Promise<any>): Subscribable<RESPONSE>;
  sendMessageAndWait(payload: REQUEST): Promise<RESPONSE | undefined>;
  sendMessageAndWait(payload: REQUEST, expiration: Promise<any>): Promise<RESPONSE | undefined>;
  clearMessages(): void;
  toJSON(): User;
}

export interface HostRoom {
  readonly lobbyCode: LobbyCode;
  readonly $lobbyGuests: Subscribable<LobbyGuest>;
  readonly $occupants: Subscribable<User>;
  readonly $occupantsExited: Subscribable<UserID>;
  getOccupants(): Promise<ReadonlyMap<UserID, Subscribable<User>>>;
  getPlayers(): Promise<ReadonlyMap<UserID, HostPlayer>>;
  getRoomPacks(): Promise<ReadonlySet<PackID>>;
  startGame(gametype: Gametype): Promise<HostGameSession>;
  close(): void;
}

export interface HostApp {
  allGames(): Promise<ReadonlySet<Game>>;
  ownedPacks(): Promise<ReadonlySet<PackID>>;
  purchasePack(id: PackID): Promise<boolean>;
  startRoom(): Promise<HostRoom>;
}

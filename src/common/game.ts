
export type Gametype = string & { readonly __Gametype: unique symbol };

export interface GameMetadata {
  readonly title: string;
  readonly subtitle: string;
  readonly description: string;
  readonly version: string;
  readonly minUsers: number;
  readonly maxUsers?: number;
}

export type PackID = string & { readonly __PackID: unique symbol };

export interface GameContentPack {
  readonly id: PackID;
  readonly title: string;
  readonly description: string;
}

export interface GameContentPacks {
  readonly base: GameContentPack;
  readonly extra: GameContentPack[];
}

export interface Game {
  readonly gametype: Gametype;
  readonly metadata: GameMetadata;
  readonly packs: GameContentPacks;
}

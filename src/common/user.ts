export type UserID = string & { readonly __UserID: unique symbol };

export type Color = string & { readonly __Color: unique symbol };

export interface User {
  readonly id: UserID;
  readonly displayName: string;
  readonly color: Color;
}

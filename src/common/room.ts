
export type LobbyCode = string & { readonly __LobbyCode: unique symbol };

export interface MessagePayload {
  readonly type: string;
}

import { GameMetadata, PackID } from "../common/game";
import { Color, User, UserID } from "../common/user";

export const userA: User = {
  id: "user-a" as UserID,
  displayName: "User A",
  color: "#AAAAAA" as Color,
};

export const userB: User = {
  id: "user-b" as UserID,
  displayName: "User B",
  color: "#BBBBBB" as Color,
};

export const userC: User = {
  id: "user-c" as UserID,
  displayName: "User C",
  color: "#CCCCCC" as Color,
};

export const gameMetadataA: GameMetadata = {
  title: "Gametype A",
  subtitle: "Gametype A Subtitle",
  description: "Gametype A Description",
  minUsers: 5,
  maxUsers: 10,
  version: "0.0.0-development"
};

export const contentPack1 = {
  id: "content-pack-1" as PackID,
  title: "Content Pack 1",
  description: "Content Pack 1 Description",
  data: { x: 1, y: 2, z: 3 },
};

export const contentPack2 = {
  id: "content-pack-2" as PackID,
  title: "Content Pack 2",
  description: "Content Pack 2 Description",
  data: { a: 0, b: 1, c: 2 },
};

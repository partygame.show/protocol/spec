import { expect } from "chai";
import { from } from "rxjs";
import { take } from "rxjs/operators";
import { LobbyCode } from "../../common/room";
import { ErrorMessage } from "../../error";
import { userA, userB } from "../common";
import { EnvironmentBuilder } from "../test-env";

export function testLobby(envBuilder: EnvironmentBuilder) {
  return describe("Lobby", () => {
    it("new room has lobby code", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const room = await env.hostApp.startRoom();
      expect(room.lobbyCode).to.be.a("string");
    });
    it("player fails to connect with invalid lobby code", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const playerA = await env.playerAppForId(userA.id).connect();
      await playerA.joinRoom("" as LobbyCode).then(() => {
        throw new Error("Expected joinRoom to throw error");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.LobbyCodeNotFound);
      });
    });
    it("player can connect with valid lobby code", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const hostRoom = await env.hostApp.startRoom();
      const playerA = await env.playerAppForId(userA.id).connect();
      const lobbyGuest_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuest = await lobbyGuest_p;
      expect(lobbyGuest.user.id).to.equal(userA.id);
    });
    it("player can join room", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const hostRoom = await env.hostApp.startRoom();
      const playerA = await env.playerAppForId(userA.id).connect();
      const lobbyGuest_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      const playerRoom_p = playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuest = await lobbyGuest_p;
      lobbyGuest.add();
      await playerRoom_p;
    });
    it("can't add player past room limit", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA, userB],
        games: [],
        roomUserLimit: 1,
      });
      const hostRoom = await env.hostApp.startRoom();
      const playerA = await env.playerAppForId(userA.id).connect();
      const lobbyGuestA_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      const playerRoomA_p = playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuestA = await lobbyGuestA_p;
      lobbyGuestA.add();
      await playerRoomA_p;
      const playerB = await env.playerAppForId(userB.id).connect();
      const lobbyGuestB_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      playerB.joinRoom(hostRoom.lobbyCode);
      const lobbyGuestB = await lobbyGuestB_p;
      expect(() => {
        lobbyGuestB.add();
      }).to.throw(ErrorMessage.RoomUserLimitReached);
    });
    it("player gets kicked from lobby", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const hostRoom = await env.hostApp.startRoom();
      const playerA = await env.playerAppForId(userA.id).connect();
      const lobbyGuest_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      const playerRoom_p = playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuest = await lobbyGuest_p;
      lobbyGuest.kick();
      await playerRoom_p.then(() => {
        throw new Error("Expected joinRoom to throw error");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.UserKickedFromLobby);
      });
    });
    it("player gets kicked from lobby, can reconnect", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const hostRoom = await env.hostApp.startRoom();
      const playerA = await env.playerAppForId(userA.id).connect();
      const lobbyGuest1_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      const playerRoom1_p = playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuest1 = await lobbyGuest1_p;
      lobbyGuest1.kick();
      await playerRoom1_p.then(() => {
        throw new Error("Expected joinRoom to throw error");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.UserKickedFromLobby);
      });
      const lobbyGuest2_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      const playerRoom2_p = playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuest2 = await lobbyGuest2_p;
      lobbyGuest2.add();
      await playerRoom2_p;
    });
    it("player gets banned from lobby", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const hostRoom = await env.hostApp.startRoom();
      const playerA = await env.playerAppForId(userA.id).connect();
      const lobbyGuest_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      const playerRoom_p = playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuest = await lobbyGuest_p;
      lobbyGuest.ban();
      await playerRoom_p.then(() => {
        throw new Error("Expected joinRoom to throw error");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.UserBannedFromRoom);
      });
    });
    it("player gets banned from lobby, cannot reconnect", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const hostRoom = await env.hostApp.startRoom();
      const playerA = await env.playerAppForId(userA.id).connect();
      const lobbyGuest1_p = from(hostRoom.$lobbyGuests).pipe(take(1)).toPromise();
      const playerRoom1_p = playerA.joinRoom(hostRoom.lobbyCode);
      const lobbyGuest1 = await lobbyGuest1_p;
      lobbyGuest1.ban();
      await playerRoom1_p.then(() => {
        throw new Error("Expected joinRoom to throw error");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.UserBannedFromRoom);
      });
      const playerRoom2_p = playerA.joinRoom(hostRoom.lobbyCode);
      await playerRoom2_p.then(() => {
        throw new Error("Expected joinRoom to throw error");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.UserBannedFromRoom);
      });
    });
  });
}

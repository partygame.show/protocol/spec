import { expect } from "chai";
import { from } from "rxjs";
import { take } from "rxjs/operators";
import sinon from "sinon";
import { MessagePayload } from "../../common/room";
import { wait } from "../../util/promise";
import { collect } from "../../util/rxjs";
import { userA } from "../common";
import { setupRoom } from "../room.spec";
import { EnvironmentBuilder } from "../test-env";

export function testMessages(envBuilder: EnvironmentBuilder) {
  const roomBuilder = setupRoom.bind(void 0, envBuilder);
  return describe("Messages", () => {
    let clock!: sinon.SinonFakeTimers;
    beforeEach(() => {
      clock = sinon.useFakeTimers();
    });
    afterEach(() => {
      clock.restore();
    });
    it("host sends request and waits for player response", async () => {
      const [, [playerA]] = await roomBuilder({
        games: [],
      }, userA);
      const message_p = from(playerA[1].$messages).pipe(take(1)).toPromise();
      const expectedRequestPayload = { type: "request", foo: 1, baz: 2 };
      const response_p = playerA[0].sendMessageAndWait(expectedRequestPayload);
      const message = await message_p;
      expect(message.payload).to.deep.equal(expectedRequestPayload);
      const expectedResponsePayload = { type: "response", foo: 2, baz: 3 };
      message.reply(expectedResponsePayload);
      await message.done();
      const response = await response_p;
      expect(response).to.deep.equal(expectedResponsePayload);
    });
    it("host sends request and waits for player response with timeout", async () => {
      const [, [playerA]] = await roomBuilder({
        games: [],
      }, userA);
      const message_p = from(playerA[1].$messages).pipe(take(1)).toPromise();
      const expectedRequestPayload = { type: "request", foo: 1, baz: 2 };
      const expiration = wait(2000);
      const response_p = playerA[0].sendMessageAndWait(expectedRequestPayload, expiration);
      const message = await message_p;
      expect(message.payload).to.deep.equal(expectedRequestPayload);
      const expectedResponsePayload = { type: "response", foo: 2, baz: 3 };
      wait(1000).then(() => message.reply(expectedResponsePayload));
      const unexpectedResponsePayload = { type: "response", foo: 0, baz: 0 };
      wait(2000).then(() => message.reply(unexpectedResponsePayload));
      clock.tick(3000);
      await message.done();
      const response = await response_p;
      expect(response).to.deep.equal(expectedResponsePayload);
    });
    it("host sends request and waits for player response after timeout", async () => {
      const [, [playerA]] = await roomBuilder({
        games: [],
      }, userA);
      const message_p = from(playerA[1].$messages).pipe(take(1)).toPromise();
      const expectedRequestPayload = { type: "request", foo: 1, baz: 2 };
      const expiration = wait(1000);
      const response_p = playerA[0].sendMessageAndWait(expectedRequestPayload, expiration);
      const message = await message_p;
      expect(message.payload).to.deep.equal(expectedRequestPayload);
      const expectedResponsePayload = { type: "response", foo: 2, baz: 3 };
      wait(2000).then(() => message.reply(expectedResponsePayload));
      clock.tick(3000);
      await message.done();
      const response = await response_p;
      expect(response).to.deep.equal(undefined);
    });
    it("host sends request and receives different player responses", async () => {
      const [, [playerA]] = await roomBuilder({
        games: [],
      }, userA);
      const message_p = from(playerA[1].$messages).pipe(take(1)).toPromise();
      const expectedRequestPayload = { type: "request", foo: 1, baz: 2 };
      const expiration = wait(3000);
      const responses_p = from(playerA[0].sendMessage(expectedRequestPayload, expiration)).pipe(collect()).toPromise();
      const message = await message_p;
      expect(message.payload).to.deep.equal(expectedRequestPayload);
      const expectedResponse1Payload = { type: "response1", foo: 2, baz: 3 };
      wait(1000).then(() => message.reply(expectedResponse1Payload));
      const expectedResponse2Payload = { type: "response2", foo: 3, baz: 4 };
      wait(2000).then(() => message.reply(expectedResponse2Payload));
      wait(4000).then(() => message.reply({ type: "late-response" }));
      clock.tick(5000);
      await message.done();
      const responses = await responses_p as MessagePayload[];
      expect(responses).to.have.length(2);
      expect(responses[0]).to.deep.equal(expectedResponse1Payload);
      expect(responses[1]).to.deep.equal(expectedResponse2Payload);
    });
    it("host sends request and receives identical player responses", async () => {
      const [, [playerA]] = await roomBuilder({
        games: [],
      }, userA);
      const message_p = from(playerA[1].$messages).pipe(take(1)).toPromise();
      const expectedRequestPayload = { type: "request", foo: 1, baz: 2 };
      const expiration = wait(3000);
      const responses_p = from(playerA[0].sendMessage(expectedRequestPayload, expiration)).pipe(collect()).toPromise();
      const message = await message_p;
      expect(message.payload).to.deep.equal(expectedRequestPayload);
      const expectedResponsePayload = { type: "response1", foo: 2, baz: 3 };
      wait(1000).then(() => message.reply(expectedResponsePayload));
      wait(2000).then(() => message.reply(expectedResponsePayload));
      wait(4000).then(() => message.reply({ type: "late-response" }));
      clock.tick(5000);
      await message.done();
      const responses = await responses_p as MessagePayload[];
      expect(responses).to.have.length(2);
      expect(responses[0]).to.deep.equal(expectedResponsePayload);
      expect(responses[1]).to.deep.equal(expectedResponsePayload);
    });
    it("host sends request and no player response sent in time", async () => {
      const [, [playerA]] = await roomBuilder({
        games: [],
      }, userA);
      const message_p = from(playerA[1].$messages).pipe(take(1)).toPromise();
      const expectedRequestPayload = { type: "request", foo: 1, baz: 2 };
      const expiration = wait(1000);
      const response_p = from(playerA[0].sendMessage(expectedRequestPayload, expiration)).pipe(collect()).toPromise();
      const message = await message_p;
      expect(message.payload).to.deep.equal(expectedRequestPayload);
      const expectedResponsePayload = { type: "response2", foo: 3, baz: 4 };
      wait(2000).then(() => message.reply(expectedResponsePayload));
      clock.tick(5000);
      await message.done();
      const responses = await response_p;
      expect(responses).to.equal(undefined);
    });
  });
}

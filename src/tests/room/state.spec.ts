import { expect } from "chai";
import { from } from "rxjs";
import { take } from "rxjs/operators";
import { collect } from "../../util/rxjs";
import { userA } from "../common";
import { setupRoom } from "../room.spec";
import { EnvironmentBuilder } from "../test-env";

export function testState(envBuilder: EnvironmentBuilder) {
  const roomBuilder = setupRoom.bind(void 0, envBuilder);
  return describe("State", () => {
    it("player receives state update, object", async () => {
      const [, [playerA]] = await roomBuilder({}, userA);
      const state_p = from(playerA[1].$state).pipe(take(1)).toPromise();
      const expected = { foo: 1, baz: 2 };
      playerA[0].state = expected;
      const state = await state_p;
      expect(state).to.deep.equal(expected);
    });
    it("player receives state update, null", async () => {
      const [, [playerA]] = await roomBuilder({}, userA);
      const state_p = from(playerA[1].$state).pipe(take(1)).toPromise();
      const expected = null;
      playerA[0].state = expected;
      const state = await state_p;
      expect(state).to.deep.equal(expected);
    });
    it("player receives multiple state updates", async () => {
      const [, [playerA]] = await roomBuilder({}, userA);
      const states_p = from(playerA[1].$state).pipe(take(4), collect()).toPromise();
      const expected1 = null;
      playerA[0].state = expected1;
      const expected2 = { foo: 1, baz: 2 };
      playerA[0].state = expected2;
      const expected3 = { foo: 1, baz: 3 };
      playerA[0].state = expected3;
      const expected4 = null;
      playerA[0].state = expected4;
      const states = await states_p as (object | null)[];
      const [state1, state2, state3, state4] = states;
      expect(state1).to.deep.equal(expected1);
      expect(state2).to.deep.equal(expected2);
      expect(state3).to.deep.equal(expected3);
      expect(state4).to.deep.equal(expected4);
    });
  });
}

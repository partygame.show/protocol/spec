import { expect } from "chai";
import { from } from "rxjs";
import { take } from "rxjs/operators";
import sinon from "sinon";
import { Gametype, PackID } from "../../common/game";
import { User, UserID } from "../../common/user";
import { ErrorMessage } from "../../error";
import { HostGameLogic, HostGameResult, HostGameSession, PlayerGameLogic, PlayerGameSession, ResourceLoader } from "../../game";
import { HostPlayer, HostRoom } from "../../host";
import { PlayerMessage, PlayerRoom } from "../../player";
import { Deferred, newDeferred } from "../../util/promise";
import { contentPack1, contentPack2, gameMetadataA, userA, userB, userC } from "../common";
import { EnvironmentBuilder, EnvironmentConfig, GameDefinition } from "../test-env";

async function buildMockLogic(result: Deferred<HostGameResult> = newDeferred()) {
  const hostLogic = {
    prepare: sinon.stub<[ReadonlyMap<UserID, HostPlayer<object>>, Map<PackID, object>, ResourceLoader]>().resolves(),
    attach: sinon.stub<[any]>().callsFake((x) => Promise.resolve(x)),
    onPlayerExit: sinon.stub<[UserID]>().callsFake((x) => Promise.resolve(x)),
    run: sinon.stub<[]>(),
    result,
  };
  const playerLogics = new Map<UserID, sinon.SinonStubbedInstance<PlayerGameLogic>>();
  const playerLogicForId = (id: UserID) => {
    const playerLogic: sinon.SinonStubbedInstance<PlayerGameLogic> = {
      prepare: sinon.stub<[User, ResourceLoader, Promise<void>]>().resolves(),
      attach: sinon.stub<[any]>().callsFake((x) => Promise.resolve(x)),
      onStateChange: sinon.stub<[object | null]>().resolves(),
      onMessage: sinon.stub<[PlayerMessage]>().resolves(),
      onClear: sinon.stub<[]>().resolves(),
    };
    playerLogics.set(id, playerLogic);
    return playerLogic as PlayerGameLogic;
  };
  return {
    hostLogic,
    playerLogicForId,
    playerLogics,
  };
}

export type AdditionalConfig = Omit<EnvironmentConfig, "host" | "users" | "games">;

async function setupGame(envBuilder: EnvironmentBuilder, game: GameDefinition, config: AdditionalConfig, host: User, ...others: User[]) {
  const users = [host, ...others];
  const env = await envBuilder({
    host: host.id,
    users,
    games: [game],
    ...config,
  });
  const hostRoom = await env.hostApp.startRoom();
  hostRoom.$lobbyGuests.subscribe((guest) => guest.add());
  const playerRooms = await Promise.all(users.map(async (user) => {
    const player = await env.playerAppForId(user.id).connect();
    return player.joinRoom(hostRoom.lobbyCode);
  }));
  return { hostRoom, playerRooms };
}

async function startGame(hostRoom: HostRoom, playerRooms: PlayerRoom[], gametype: Gametype): Promise<[HostGameSession, PlayerGameSession[]]> {
  const playerGames_p = Promise.all(playerRooms.map(async (playerRoom) => from(playerRoom.$games).pipe(take(1)).toPromise()));
  const hostGame = await hostRoom.startGame(gametype);
  const playerGames = await playerGames_p;
  return [hostGame, playerGames];
}

export function testGames(envBuilder: EnvironmentBuilder) {
  return describe("Games", () => {
    it("are prepared with expected arguments", async () => {
      const { hostLogic, playerLogicForId, playerLogics } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      const [hostGame, [playerA, playerB, playerC]] = await startGame(hostRoom, playerRooms, gametype);
      expect(hostGame).to.equal(hostLogic);
      expect(hostLogic.prepare.callCount).to.equal(1);
      expect(hostLogic.prepare.args[0][0]).to.have.lengthOf(3);
      expect(hostLogic.prepare.args[0][0].has(userA.id));
      expect(hostLogic.prepare.args[0][0].has(userB.id));
      expect(hostLogic.prepare.args[0][0].has(userC.id));
      expect(hostLogic.prepare.args[0][1]).to.have.property("size", 2);
      expect(hostLogic.prepare.args[0][1].get(contentPack1.id)).to.deep.equal(contentPack1.data);
      expect(hostLogic.prepare.args[0][1].get(contentPack2.id)).to.deep.equal(contentPack2.data);
      const playerGameA = playerLogics.get(userA.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      expect(playerA).to.equal(playerGameA);
      expect(playerGameA.prepare.callCount).to.equal(1);
      expect(playerGameA.prepare.args[0][0].id).to.equal(userA.id);
      const playerGameB = playerLogics.get(userB.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      expect(playerB).to.equal(playerGameB);
      expect(playerGameB.prepare.callCount).to.equal(1);
      expect(playerGameB.prepare.args[0][0].id).to.equal(userB.id);
      const playerGameC = playerLogics.get(userC.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      expect(playerC).to.equal(playerGameC);
      expect(playerGameC.prepare.callCount).to.equal(1);
      expect(playerGameC.prepare.args[0][0].id).to.equal(userC.id);
    });
    it("are prepared with valid resource loaders", async () => {
      const { hostLogic, playerLogicForId, playerLogics } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
        resources: {
          "dummy/file.txt": "dummy file contents",
          "dummy/file.json": "{\"bool\": true}",
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      const [hostGame, [playerA]] = await startGame(hostRoom, playerRooms, gametype);
      expect(hostGame).to.equal(hostLogic);
      expect(hostLogic.prepare.callCount).to.equal(1);
      const hostResourceLoader = hostLogic.prepare.args[0][2];
      const hostResourceText = await hostResourceLoader.load("dummy/file.txt");
      expect(hostResourceText).to.equal("dummy file contents");
      const hostResourceJson = await hostResourceLoader.load("dummy/file.json", "json");
      expect(hostResourceJson).to.deep.equal({ bool: true });
      const playerGameA = playerLogics.get(userA.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      expect(playerA).to.equal(playerGameA);
      expect(playerGameA.prepare.callCount).to.equal(1);
      const playerResourceLoader = playerGameA.prepare.args[0][1];
      const playerResourceText = await playerResourceLoader.load("dummy/file.txt");
      expect(playerResourceText).to.equal("dummy file contents");
      const playerResourceJson = await playerResourceLoader.load("dummy/file.json", "json");
      expect(playerResourceJson).to.deep.equal({ bool: true });
    });
    it("player gets promise for host end of game", async () => {
      const { hostLogic, playerLogicForId, playerLogics } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
        resources: {
          "dummy/file.txt": "dummy file contents",
          "dummy/file.json": "{\"bool\": true}",
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      const [, [playerA]] = await startGame(hostRoom, playerRooms, gametype);
      const playerGameA = playerLogics.get(userA.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      expect(playerA).to.equal(playerGameA);
      expect(playerGameA.prepare.callCount).to.equal(1);
      const playerEnd_p = playerGameA.prepare.args[0][2];
      await hostLogic.result.resolve({ scores: new Map() });
      await playerEnd_p;
    });
    it("fails to start another game before first ends", async () => {
      const { hostLogic, playerLogicForId } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      await startGame(hostRoom, playerRooms, gametype);
      await startGame(hostRoom, playerRooms, gametype).then(() => {
        throw new Error("Expected startGame to throw exception");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.GameAlreadyInProgress);
      });
    });
    it("fails to start an invalid gametype", async () => {
      const { hostLogic, playerLogicForId } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      await startGame(hostRoom, playerRooms, "invalid-gametype" as Gametype).then(() => {
        throw new Error("Expected startGame to throw exception");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.GameNotFound);
      });
    });
    it("fails to start an unowned game", async () => {
      const { hostLogic, playerLogicForId } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      await startGame(hostRoom, playerRooms, gametype).then(() => {
        throw new Error("Expected startGame to throw exception");
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.BasePackUnowned);
      });
    });
    it("starts game owned by non-host user", async () => {
      const { hostLogic, playerLogicForId } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack2.id], [userB.id]: [contentPack1.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      const [hostGame] = await startGame(hostRoom, playerRooms, gametype);
      expect(hostGame).to.equal(hostLogic);
      expect(hostLogic.prepare.callCount).to.equal(1);
      expect(hostLogic.prepare.args[0][1]).to.have.property("size", 2);
      expect(hostLogic.prepare.args[0][1].get(contentPack1.id)).to.deep.equal(contentPack1.data);
      expect(hostLogic.prepare.args[0][1].get(contentPack2.id)).to.deep.equal(contentPack2.data);
    });
    it("starts another game after first ends", async () => {
      const { hostLogic, playerLogicForId } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      await startGame(hostRoom, playerRooms, gametype);
      await hostLogic.result.resolve({ scores: new Map() });
      await startGame(hostRoom, playerRooms, gametype);
    });
    it("calls logic when player leaves", async () => {
      const { hostLogic, playerLogicForId } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 3,
      }, userA, userB, userC);
      await startGame(hostRoom, playerRooms, gametype);
      const exited_p = from(hostRoom.$occupantsExited).pipe(take(1)).toPromise();
      playerRooms[2].exit();
      const exited = await exited_p;
      expect(hostLogic.onPlayerExit.callCount).to.equal(1);
      expect(hostLogic.onPlayerExit.args[0][0]).to.equal(exited);
    });
    it("transmit state updates", async () => {
      const { hostLogic, playerLogicForId, playerLogics } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id], [userB.id]: [contentPack2.id] },
        roomUserLimit: 2,
      }, userA, userB);
      await startGame(hostRoom, playerRooms, gametype);
      const playerGameB = playerLogics.get(userB.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      let count = 0;
      const stateChangeCall1 = newDeferred<void>();
      const stateChangeCall2 = newDeferred<void>();
      playerGameB.onStateChange.callsFake(async () => {
        count++;
        if (count === 1) {
          stateChangeCall1.resolve();
        } else if (count === 2) {
          stateChangeCall2.resolve();
        } else {
          throw new Error("onStateChange called more than expected");
        }
      });
      const expectedState1 = { foo: "baz" };
      const expectedState2 = { baz: "foo" };
      const playerState_p = from(playerRooms[1].$state).pipe(take(2)).toPromise();
      const players = hostLogic.prepare.args[0][0];
      const playerB = players.get(userB.id);
      if (!playerB) {
        throw new Error("No player exists for userB");
      }
      playerB.state = expectedState1;
      playerB.state = expectedState2;
      await playerState_p;
      await stateChangeCall1;
      await stateChangeCall2;
      expect(playerGameB.onStateChange.callCount).to.equal(2);
      expect(playerGameB.onStateChange.args[0][0]).to.equal(expectedState1);
      expect(playerGameB.onStateChange.args[1][0]).to.equal(expectedState2);
    });
    it("transmit clear message signal", async () => {
      const { hostLogic, playerLogicForId, playerLogics } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id, contentPack2.id] },
        roomUserLimit: 2,
      }, userA, userB);
      await startGame(hostRoom, playerRooms, gametype);
      const playerClear_p = from(playerRooms[1].$onClear).pipe(take(1)).toPromise();
      const players = hostLogic.prepare.args[0][0];
      const playerB = players.get(userB.id);
      if (!playerB) {
        throw new Error("No player exists for userB");
      }
      playerB.clearMessages();
      await playerClear_p;
      const playerGameB = playerLogics.get(userB.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      expect(playerGameB.onClear.callCount).to.equal(1);
    });
    it("transmit messages", async () => {
      const { hostLogic, playerLogicForId, playerLogics } = await buildMockLogic();
      const gametype = "game-a" as Gametype;
      const { hostRoom, playerRooms } = await setupGame(envBuilder, {
        gametype,
        metadata: gameMetadataA,
        packs: {
          base: contentPack1,
          extra: [contentPack2],
        },
        content: [
          [contentPack1.id, contentPack1.data],
          [contentPack2.id, contentPack2.data],
        ],
        logic: {
          host: () => hostLogic as HostGameLogic,
          player: playerLogicForId,
        },
      }, {
        ownedPacks: { [userA.id]: [contentPack1.id, contentPack2.id] },
        roomUserLimit: 2,
      }, userA, userB);
      await startGame(hostRoom, playerRooms, gametype);
      const playerMessages_p = from(playerRooms[1].$messages).pipe(take(2)).toPromise();
      const dummyReply = { type: "reply", reply: "foo" };
      for (const playerLogic of playerLogics.values()) {
        playerLogic.onMessage.callsFake(async (message) => {
          message.reply(dummyReply);
        });
      }
      const expectedPayload1 = { type: "expected-1", foo: "baz" };
      const expectedPayload2 = { type: "expected-2", foo: "baz" };
      const players = hostLogic.prepare.args[0][0];
      const playerB = players.get(userB.id);
      if (!playerB) {
        throw new Error("No player exists for userB");
      }
      await Promise.all([
        playerB.sendMessageAndWait(expectedPayload1),
        playerB.sendMessageAndWait(expectedPayload2),
      ]);
      await playerMessages_p;
      const playerGameB = playerLogics.get(userB.id) as sinon.SinonStubbedInstance<PlayerGameLogic>;
      expect(playerGameB.onMessage.callCount).to.equal(2);
      expect(playerGameB.onMessage.args[0][0].payload).to.deep.equal(expectedPayload1);
      expect(playerGameB.onMessage.args[1][0].payload).to.deep.equal(expectedPayload2);
    });
  });
}

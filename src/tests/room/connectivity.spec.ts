import { expect } from "chai";
import { from } from "rxjs";
import { take } from "rxjs/operators";
import { userA, userB } from "../common";
import { setupRoom } from "../room.spec";
import { EnvironmentBuilder } from "../test-env";

export function testConnectivity(envBuilder: EnvironmentBuilder) {
  const roomBuilder = setupRoom.bind(void 0, envBuilder);
  it("player reconnects to room", async () => {
    const env = await envBuilder({
      host: userA.id,
      users: [userA, userB],
    });
    const playerApp = env.playerAppForId(userB.id);
    const player = await playerApp.connect();
    const hostRoom = await env.hostApp.startRoom();
    from(hostRoom.$lobbyGuests).pipe(take(1)).subscribe((guest) => guest.add());
    await player.joinRoom(hostRoom.lobbyCode);
    const playerReconnect = await playerApp.connect();
    await playerReconnect.joinRoom(hostRoom.lobbyCode);
  });
  it("player leaves room", async () => {
    const env = await envBuilder({
      host: userA.id,
      users: [userA, userB],
    });
    const playerApp = env.playerAppForId(userB.id);
    const player = await playerApp.connect();
    const hostRoom = await env.hostApp.startRoom();
    hostRoom.$lobbyGuests.subscribe((guest) => guest.add());
    const playerRoom = await player.joinRoom(hostRoom.lobbyCode);
    const playersBefore = await hostRoom.getPlayers();
    expect(playersBefore.get(userB.id)).to.not.equal(undefined);
    const exited_p = from(hostRoom.$occupantsExited).pipe(take(1)).toPromise();
    playerRoom.exit();
    const exited = await exited_p;
    expect(exited).to.equal(userB.id);
    const playersAfter = await hostRoom.getPlayers();
    expect(playersAfter.get(userB.id)).to.equal(undefined);
  });
  it("host closes room", async () => {
    const [hostRoom, [, playerB]] = await roomBuilder({}, userA, userB);
    const playersBefore = await hostRoom.getPlayers();
    expect(playersBefore.get(userB.id)).to.not.equal(undefined);
    const closed_p = playerB[1].closed;
    hostRoom.close();
    await closed_p;
  });
}

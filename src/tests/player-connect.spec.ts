import { expect } from "chai";
import { Color } from "../common/user";
import { userA } from "./common";
import { EnvironmentBuilder } from "./test-env";

export function testPlayerConnect(envBuilder: EnvironmentBuilder) {
  return describe("Player Connection", () => {
    it("connect to existing player", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const playerApp = env.playerAppForId(userA.id);
      const player = await playerApp.connect();
      expect(player.user).to.have.property("id", userA.id);
      expect(player.user).to.have.property("displayName", userA.displayName);
      expect(player.user).to.have.property("color", userA.color);
    });
    it("connect to existing player, overwrite displayName on connect", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const playerApp = env.playerAppForId(userA.id);
      const newDisplayName = "new-display-name";
      const player = await playerApp.connect(newDisplayName);
      expect(player.user).to.have.property("id", userA.id);
      expect(player.user).to.have.property("color", userA.color);
      expect(player.user).to.have.property("displayName", newDisplayName);
    });
    it("connect to existing player, overwrite color on connect", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const playerApp = env.playerAppForId(userA.id);
      const newColor = "#000000";
      const player = await playerApp.connect(void 0, newColor);
      expect(player.user).to.have.property("id", userA.id);
      expect(player.user).to.have.property("displayName", userA.displayName);
      expect(player.user).to.have.property("color", newColor);
    });
    it("connect to existing player, overwrite displayName after connect", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const playerApp = env.playerAppForId(userA.id);
      const player = await playerApp.connect();
      const newDisplayName = "new-display-name";
      await player.setDisplayName(newDisplayName);
      expect(player.user).to.have.property("id", userA.id);
      expect(player.user).to.have.property("displayName", newDisplayName);
      expect(player.user).to.have.property("color", userA.color);
    });
    it("connect to existing player, overwrite color after connect", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const playerApp = env.playerAppForId(userA.id);
      const player = await playerApp.connect();
      const newColor = "#000000" as Color;
      await player.setColor(newColor);
      expect(player.user).to.have.property("id", userA.id);
      expect(player.user).to.have.property("displayName", userA.displayName);
      expect(player.user).to.have.property("color", newColor);
    });
  });
}

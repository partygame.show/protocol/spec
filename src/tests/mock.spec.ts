import { buildTestEnvironment } from "./mock";
import { runTestSuite } from "./test-env";

runTestSuite(async (config) => buildTestEnvironment(config));

import { Game, PackID } from "../common/game";
import { User, UserID } from "../common/user";
import { HostGameLogic, PlayerGameLogic } from "../game";
import { HostApp } from "../host";
import { PlayerApp } from "../player";
import { testAllGames } from "./all-games.spec";
import { testPlayerConnect } from "./player-connect.spec";
import { testRoom } from "./room.spec";

export interface GameLogicDefinition {
  host(): HostGameLogic;
  player(id: UserID): PlayerGameLogic;
}

export interface GameDefinition extends Game {
  readonly content?: [PackID, object][];
  readonly logic?: GameLogicDefinition;
  readonly resources?: { [uri: string]: string };
}

export interface EnvironmentConfig {
  host: UserID;
  users: User[];
  games?: GameDefinition[];
  ownedPacks?: { [userID: string]: PackID[] };
  roomUserLimit?: number;
  testPurchaseError?: boolean;
}

export interface TestEnvironment {
  hostApp: HostApp;
  playerAppForId(userId: UserID): PlayerApp;
}

export type EnvironmentBuilder = (config: EnvironmentConfig) => Promise<TestEnvironment>;

export function runTestSuite(envBuilder: EnvironmentBuilder) {
  testAllGames(envBuilder);
  testPlayerConnect(envBuilder);
  testRoom(envBuilder);
}

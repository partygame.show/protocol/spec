import { expect } from "chai";
import { Gametype } from "../common/game";
import { ErrorMessage } from "../error";
import { contentPack1, contentPack2, gameMetadataA, userA } from "./common";
import { EnvironmentBuilder } from "./test-env";

export function testAllGames(envBuilder: EnvironmentBuilder) {
  return describe("Game List", () => {
    it("no games", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
      });
      const games = await env.hostApp.allGames();
      expect(games).to.have.property("size", 0);
      const owned = await env.hostApp.ownedPacks();
      expect(owned).to.have.property("size", 0);
    });
    it("one game", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [
          {
            gametype: "game-a" as Gametype,
            metadata: gameMetadataA,
            packs: {
              base: contentPack1,
              extra: [],
            },
            content: [[contentPack1.id, contentPack1.data]],
          },
        ],
      });
      const games = await env.hostApp.allGames();
      expect(games).to.have.property("size", 1);
      const game = Array.from(games)[0];
      expect(game.gametype).to.equal("game-a");
      expect(game.metadata).to.deep.equal(gameMetadataA);
      expect(game.packs.base).to.have.property("id", contentPack1.id);
      expect(game.packs.extra).to.have.length(0);
      const owned = await env.hostApp.ownedPacks();
      expect(owned).to.have.property("size", 0);
    });
    it("one game owned by host", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [
          {
            gametype: "game-a" as Gametype,
            metadata: gameMetadataA,
            packs: {
              base: contentPack1,
              extra: [],
            },
            content: [[contentPack1.id, contentPack1.data]],
          },
        ],
        ownedPacks: {
          [userA.id]: [contentPack1.id],
        },
      });
      const games = await env.hostApp.allGames();
      expect(games).to.have.property("size", 1);
      const game = Array.from(games)[0];
      expect(game.gametype).to.equal("game-a");
      expect(game.metadata).to.deep.equal(gameMetadataA);
      expect(game.packs.base).to.have.property("id", contentPack1.id);
      expect(game.packs.extra).to.have.length(0);
      const owned = await env.hostApp.ownedPacks();
      expect(owned).to.have.property("size", 1);
      expect(owned.has(contentPack1.id));
    });
    it("one game with two packs owned by host", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [
          {
            gametype: "game-a" as Gametype,
            metadata: gameMetadataA,
            packs: {
              base: contentPack1,
              extra: [contentPack2],
            },
            content: [
              [contentPack1.id, contentPack1.data],
              [contentPack2.id, contentPack2.data],
            ],
          },
        ],
        ownedPacks: {
          [userA.id]: [contentPack1.id, contentPack2.id],
        },
      });
      const games = await env.hostApp.allGames();
      expect(games).to.have.property("size", 1);
      const game = Array.from(games)[0];
      expect(game.gametype).to.equal("game-a");
      expect(game.metadata).to.deep.equal(gameMetadataA);
      expect(game.packs.base).to.have.property("id", contentPack1.id);
      expect(game.packs.extra).to.have.length(1);
      expect(game.packs.extra[0]).to.have.property("id", contentPack2.id);
      const owned = await env.hostApp.ownedPacks();
      expect(owned).to.have.property("size", 2);
      expect(owned.has(contentPack1.id));
      expect(owned.has(contentPack2.id));
    });
    it("one game, base purchased by host", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [
          {
            gametype: "game-a" as Gametype,
            metadata: gameMetadataA,
            packs: {
              base: contentPack1,
              extra: [],
            },
            content: [[contentPack1.id, contentPack1.data]],
          },
        ],
        ownedPacks: {
          [userA.id]: [],
        },
      });
      const ownedBefore = await env.hostApp.ownedPacks();
      expect(ownedBefore).to.have.property("size", 0);
      await env.hostApp.purchasePack(contentPack1.id);
      const ownedAfter = await env.hostApp.ownedPacks();
      expect(ownedAfter).to.have.property("size", 1);
      expect(ownedAfter.has(contentPack1.id));
    });
    it("one game owned by host, extra purchased by host", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [
          {
            gametype: "game-a" as Gametype,
            metadata: gameMetadataA,
            packs: {
              base: contentPack1,
              extra: [contentPack2],
            },
            content: [
              [contentPack1.id, contentPack1.data],
              [contentPack2.id, contentPack2.data],
            ],
          },
        ],
        ownedPacks: {
          [userA.id]: [contentPack1.id],
        },
      });
      const ownedBefore = await env.hostApp.ownedPacks();
      expect(ownedBefore).to.have.property("size", 1);
      expect(ownedBefore.has(contentPack1.id));
      await env.hostApp.purchasePack(contentPack2.id);
      const ownedAfter = await env.hostApp.ownedPacks();
      expect(ownedAfter).to.have.property("size", 2);
      expect(ownedAfter.has(contentPack1.id));
      expect(ownedAfter.has(contentPack2.id));
    });
    it("one game not owned by host, cannot purchase extra", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [
          {
            gametype: "game-a" as Gametype,
            metadata: gameMetadataA,
            packs: {
              base: contentPack1,
              extra: [contentPack2],
            },
            content: [
              [contentPack1.id, contentPack1.data],
              [contentPack2.id, contentPack2.data],
            ],
          },
        ],
        ownedPacks: {
          [userA.id]: [],
        },
      });
      const ownedBefore = await env.hostApp.ownedPacks();
      expect(ownedBefore).to.have.property("size", 0);
      await env.hostApp.purchasePack(contentPack2.id).then(() => {
        throw new Error(`Expected purchasePack("${contentPack2.id}") to throw exception`);
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.BasePackUnowned);
      });
      const ownedAfter = await env.hostApp.ownedPacks();
      expect(ownedAfter).to.have.property("size", 0);
    });
    it("cannot purchase pack that does not exist", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [],
        ownedPacks: {
          [userA.id]: [],
        },
      });
      const ownedBefore = await env.hostApp.ownedPacks();
      expect(ownedBefore).to.have.property("size", 0);
      await env.hostApp.purchasePack(contentPack1.id).then(() => {
        throw new Error(`Expected purchasePack("${contentPack1.id}") to throw exception`);
      }, (err) => {
        expect(err).to.be.instanceof(Error);
        expect(err).to.have.property("message", ErrorMessage.PackNotFound);
      });
      const ownedAfter = await env.hostApp.ownedPacks();
      expect(ownedAfter).to.have.property("size", 0);
    });
    it("cannot purchase pack due to error", async () => {
      const env = await envBuilder({
        host: userA.id,
        users: [userA],
        games: [
          {
            gametype: "game-a" as Gametype,
            metadata: gameMetadataA,
            packs: {
              base: contentPack1,
              extra: [contentPack2],
            },
            content: [
              [contentPack1.id, contentPack1.data],
              [contentPack2.id, contentPack2.data],
            ],
          },
        ],
        ownedPacks: {
          [userA.id]: [contentPack1.id],
        },
        testPurchaseError: true,
      });
      const ownedBefore = await env.hostApp.ownedPacks();
      expect(ownedBefore).to.have.property("size", 1);
      expect(ownedBefore.has(contentPack1.id));
      await env.hostApp.purchasePack(contentPack1.id).then(() => {
        throw new Error(`Expected purchasePack("${contentPack1.id}") to throw exception`);
      }, (err) => {
        expect(err).to.be.instanceof(Error);
      });
      const ownedAfter = await env.hostApp.ownedPacks();
      expect(ownedAfter).to.have.property("size", 1);
      expect(ownedBefore.has(contentPack1.id));
    });
  });
}

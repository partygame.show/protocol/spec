import { User } from "../common/user";
import { HostPlayer, HostRoom } from "../host";
import { PlayerRoom } from "../player";
import { testConnectivity } from "./room/connectivity.spec";
import { testGames } from "./room/games.spec";
import { testLobby } from "./room/lobby.spec";
import { testMessages } from "./room/messages.spec";
import { testState } from "./room/state.spec";
import { EnvironmentBuilder, EnvironmentConfig } from "./test-env";


export type RoomConfig = Omit<EnvironmentConfig, "host" | "users">;
export type PlayerContext = [HostPlayer<object>, PlayerRoom];
export type RoomBuilder = (config: RoomConfig, host: User, ...others: User[]) => Promise<[HostRoom, PlayerContext[]]>;

export async function setupRoom(envBuilder: EnvironmentBuilder, config: RoomConfig, host: User, ...others: User[]): Promise<[HostRoom, PlayerContext[]]> {
  const users = [host, ...others];
  const env = await envBuilder({
    host: host.id,
    users,
    ...config,
  });
  const hostRoom = await env.hostApp.startRoom();
  hostRoom.$lobbyGuests.subscribe((guest) => guest.add());
  const playerRooms = await Promise.all(users.map(async (user) => {
    const player = await env.playerAppForId(user.id).connect();
    return player.joinRoom(hostRoom.lobbyCode);
  }));
  const hostPlayers = await hostRoom.getPlayers();
  return [hostRoom, playerRooms.map<PlayerContext>((playerRoom, i) => {
    const hostPlayer = hostPlayers.get(users[i].id);
    if (!hostPlayer) {
      throw new Error("Failed to find host player for user");
    }
    return [hostPlayer, playerRoom];
  })];
}


export function testRoom(envBuilder: EnvironmentBuilder) {
  return describe("Room", () => {
    testLobby(envBuilder);
    testConnectivity(envBuilder);
    testState(envBuilder);
    testMessages(envBuilder);
    testGames(envBuilder);
  });
}

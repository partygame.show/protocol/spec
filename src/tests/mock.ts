import { from, Observable, Subject } from "rxjs";
import { concatMap, take, takeUntil } from "rxjs/operators";
import { Gametype, PackID } from "../common/game";
import { LobbyCode, MessagePayload } from "../common/room";
import { Color, User, UserID } from "../common/user";
import { ErrorMessage } from "../error";
import { HostGameLogic, HostGameSession, PlayerGameSession, ResourceLoader } from "../game";
import { HostApp, HostPlayer, HostRoom, LobbyGuest } from "../host";
import { Player, PlayerApp, PlayerMessage, PlayerRoom } from "../player";
import { Deferred, newDeferred } from "../util/promise";
import { EnvironmentConfig, GameDefinition, GameLogicDefinition, TestEnvironment } from "./test-env";

type ResourceLoaderLoader = (gametype: Gametype) => ResourceLoader;

export class MockUser implements User {
  private _id: UserID;
  private _displayName: string;
  private _color: Color;
  private _changes = new Subject<User>();
  constructor(user: User) {
    this._id = user.id;
    this._displayName = user.displayName;
    this._color = user.color;
  }
  get id(): UserID {
    return this._id;
  }
  get displayName(): string {
    return this._displayName;
  }
  set displayName(value: string) {
    this._displayName = value;
    this._emitChange();
  }
  get color(): Color {
    return this._color;
  }
  set color(value: Color) {
    this._color = value;
    this._emitChange();
  }
  private _emitChange() {
    this._changes.next({
      id: this._id,
      displayName: this._displayName,
      color: this._color,
    });
  }
  asObservable(): Observable<User> {
    return this._changes;
  }
}

export class MockPlayerApp implements PlayerApp {
  private _user: MockUser;
  private _rooms: Map<LobbyCode, MockRoom>;
  constructor(user: MockUser, rooms: Map<LobbyCode, MockRoom>) {
    this._user = user;
    this._rooms = rooms;
  }
  async connect(displayName?: string, color?: Color): Promise<Player> {
    if (displayName) {
      this._user.displayName = displayName;
    }
    if (color) {
      this._user.color = color;
    }
    return {
      setDisplayName: async (value: string) => {
        this._user.displayName = value;
      },
      setColor: async (value: Color) => {
        this._user.color = value;
      },
      user: this._user,
      joinRoom: async (lobbyCode: LobbyCode) => {
        const room = this._rooms.get(lobbyCode);
        if (!room) {
          throw new Error(ErrorMessage.LobbyCodeNotFound);
        }
        return room.addUserToLobby(this._user);
      },
    };
  }
}

export class MockPlayerMessage<REQUEST extends MessagePayload, RESPONSE extends MessagePayload> implements PlayerMessage<REQUEST, RESPONSE> {
  private _request: REQUEST;
  private _responseInput = new Subject<RESPONSE>();
  private _responseOutput: Observable<RESPONSE>;
  private _done: Promise<any>;
  constructor(request: REQUEST, expiration?: Promise<any>) {
    this._request = request;
    if (expiration) {
      this._responseOutput = this._responseInput.pipe(takeUntil(from(expiration)));
    } else {
      this._responseOutput = this._responseInput.pipe(take(1));
    }
    this._done = this._responseOutput.toPromise();
  }
  get payload(): REQUEST {
    return this._request;
  }
  reply(payload: RESPONSE): void {
    this._responseInput.next(payload);
  }
  async done(): Promise<void> {
    await this._done;
  }
  responses(): Observable<RESPONSE> {
    return this._responseOutput;
  }
}

export class MockPlayer<PLAYER_STATE extends {}> implements HostPlayer<PLAYER_STATE>, PlayerRoom {
  private _user: MockUser;
  private _gameLoader: GameLoader;
  private _getResourceLoader: ResourceLoaderLoader;
  private _games: Observable<[Gametype, Promise<void>]>;
  private _clearSignal = new Subject<void>();
  private _state: PLAYER_STATE | null = null;
  private _stateSubject = new Subject<PLAYER_STATE | null>();
  private _messageSubject = new Subject<MockPlayerMessage<any, any>>();
  private _exited = newDeferred<void>();
  private _roomClosed: Deferred<void>;
  constructor(user: MockUser, gameLoader: GameLoader, getResourceLoader: ResourceLoaderLoader, games: Observable<[Gametype, Promise<void>]>, roomClosed: Deferred<void>) {
    this._user = user;
    this._gameLoader = gameLoader;
    this._getResourceLoader = getResourceLoader;
    this._games = games;
    this._roomClosed = roomClosed;
  }
  get user(): MockUser {
    return this._user;
  }
  get state(): PLAYER_STATE | null {
    return this._state;
  }
  set state(value: PLAYER_STATE | null) {
    this._state = value;
    this._stateSubject.next(value);
  }
  get $state(): Observable<PLAYER_STATE | null> {
    return this._stateSubject;
  }
  sendMessage<REQUEST extends MessagePayload, RESPONSE extends MessagePayload>(payload: REQUEST, expiration: Promise<any>): Observable<RESPONSE> {
    const message = new MockPlayerMessage<REQUEST, RESPONSE>(payload, expiration);
    this._messageSubject.next(message);
    return message.responses();
  }
  sendMessageAndWait<REQUEST extends MessagePayload, RESPONSE extends MessagePayload>(payload: REQUEST, expiration?: Promise<any>): Promise<RESPONSE | undefined> {
    const message = new MockPlayerMessage<REQUEST, RESPONSE>(payload, expiration);
    const response_p = message.responses().pipe(take(1)).toPromise();
    this._messageSubject.next(message);
    return response_p;
  }
  clearMessages(): void {
    this._clearSignal.next();
  }
  toJSON(): User {
    return this._user;
  }
  get id(): UserID {
    return this._user.id;
  }
  get displayName(): string {
    return this._user.displayName;
  }
  get color(): Color {
    return this._user.color;
  }
  get closed(): Promise<void> {
    return this._roomClosed;
  }
  exit(): void {
    this._exited.resolve();
  }
  get $exited(): Promise<void> {
    return this._exited;
  }
  get $games(): Observable<PlayerGameSession> {
    return this._games.pipe(concatMap(async ([gametype, end]) => {
      const logic = this._gameLoader.load(gametype).player(this._user.id);
      await logic.prepare(this._user, this._getResourceLoader(gametype), end);
      let currentAction: Promise<any> = Promise.resolve();
      const subscriptions = [
        this.$state.subscribe((state) => {
          currentAction = currentAction.then(() => logic.onStateChange(state));
        }),
        this.$messages.subscribe((message) => {
          currentAction = currentAction.then(() => logic.onMessage(message));
        }),
        this.$onClear.subscribe(() => logic.onClear()),
      ];
      end.then(() => {
        subscriptions.forEach((sub) => sub.unsubscribe());
      });
      return logic;
    }));
  }
  get $messages(): Observable<PlayerMessage<MessagePayload, MessagePayload>> {
    return this._messageSubject;
  }
  get $onClear(): Observable<void> {
    return this._clearSignal;
  }
}

export class MockRoom implements HostRoom {
  private _gameLoader: GameLoader;
  private _getResourceLoader: ResourceLoaderLoader;
  private _userLimit: number | undefined;
  private _lobbyCode: LobbyCode;
  private _lobbyGuests = new Subject<LobbyGuest>();
  private _blacklist = new Set<UserID>();
  private _occupants = new Subject<MockUser>();
  private _occupantsExited = new Subject<UserID>();
  private _players = new Map<UserID, MockPlayer<any>>();
  private _games = new Subject<[Gametype, Promise<void>]>();
  private _activeLogic: HostGameLogic | null = null;
  private _closed = newDeferred<void>();
  constructor(gameLoader: GameLoader, getResourceLoader: ResourceLoaderLoader, userLimit: number | undefined) {
    this._gameLoader = gameLoader;
    this._getResourceLoader = getResourceLoader;
    this._userLimit = userLimit;
    this._lobbyCode = Math.random().toString(36).substring(2, 7) as LobbyCode;
  }
  get lobbyCode(): LobbyCode {
    return this._lobbyCode;
  }
  private _removeUser(id: UserID) {
    this._players.delete(id);
    this._occupantsExited.next(id);
  }
  addUserToLobby(user: MockUser): Promise<MockPlayer<any>> {
    if (this._blacklist.has(user.id)) {
      return Promise.reject(new Error(ErrorMessage.UserBannedFromRoom));
    }
    return new Promise<MockPlayer<any>>((resolve, reject) => {
      const guest: LobbyGuest = {
        user,
        add: () => {
          if (typeof this._userLimit === "number" && this._players.size >= this._userLimit && !this._players.has(user.id)) {
            throw new Error(ErrorMessage.RoomUserLimitReached);
          }
          const player = new MockPlayer(user, this._gameLoader, this._getResourceLoader, this._games, this._closed);
          player.$exited.then(() => this._removeUser(user.id));
          this._players.set(user.id, player);
          this._occupants.next(user);
          resolve(player);
        },
        kick: () => reject(new Error(ErrorMessage.UserKickedFromLobby)),
        ban: () => {
          this._blacklist.add(user.id);
          reject(new Error(ErrorMessage.UserBannedFromRoom));
        },
      };
      if (this._players.has(user.id)) {
        guest.add();
        return;
      }
      this._lobbyGuests.next(guest);
    });
  }
  get $lobbyGuests(): Observable<LobbyGuest> {
    return this._lobbyGuests;
  }
  get $occupants(): Observable<User> {
    return this._occupants;
  }
  get $occupantsExited(): Observable<UserID> {
    return this._occupantsExited;
  }
  async getOccupants(): Promise<Map<UserID, Observable<User>>> {
    return new Map(Array.from(this._players.entries()).map(([id, player]) => [id, player.user.asObservable()]));
  }
  async getPlayers<PLAYER_STATE extends {}>(): Promise<ReadonlyMap<UserID, HostPlayer<PLAYER_STATE>>> {
    return this._players;
  }
  async getRoomPacks(): Promise<Set<PackID>> {
    const players = await this.getPlayers();
    const users = new Set(players.values());
    return new Set(this._gameLoader.ownedPacks(users).keys());
  }
  async startGame(gametype: Gametype): Promise<HostGameSession> {
    if (this._activeLogic) {
      throw new Error(ErrorMessage.GameAlreadyInProgress);
    }
    const players = await this.getPlayers();
    const users = new Set(players.values());
    const ownedPacks = this._gameLoader.ownedPacks(users, gametype);
    const logic = this._gameLoader.load(gametype, ownedPacks).host();
    this._activeLogic = logic;
    const subscriptions = [
      this._occupantsExited.subscribe((id) => logic.onPlayerExit(id)),
    ];
    const end = logic.result.then(() => {
      this._activeLogic = null;
      subscriptions.forEach((sub) => sub.unsubscribe());
    });
    this._games.next([gametype, end]);
    await logic.prepare(players, ownedPacks, this._getResourceLoader(gametype));
    return logic;
  }
  close() {
    this._closed.resolve();
  }
}

interface GameLoader {
  ownedPacks(users: ReadonlySet<User>, gametype?: Gametype): Map<PackID, object>;
  load(gametype: Gametype, ownedPacks?: Map<PackID, object>): GameLogicDefinition;
}

export function buildTestEnvironment(config: EnvironmentConfig): TestEnvironment {
  const rooms = new Map<LobbyCode, MockRoom>();

  const gameLoader: GameLoader = {
    ownedPacks(users: ReadonlySet<User>, gametype?: Gametype) {
      if (!config.ownedPacks || !config.games) {
        return new Map();
      }
      let gamePacks: [PackID, object][] = [];
      if (gametype) {
        const game = config.games.find((game) => game.gametype === gametype);
        if (!game) {
          throw new Error(ErrorMessage.GameNotFound);
        }
        gamePacks = game.content || [];
      } else {
        gamePacks = config.games.reduce<[PackID, object][]>((all, game) => [...all, ...(game.content || [])], []);
      }
      const ownedPackIds = Array.from(users).reduce((set, user) => {
        if (config.ownedPacks && config.ownedPacks[user.id]) {
          (config.ownedPacks[user.id] || []).forEach((packId) => set.add(packId));
        }
        return set;
      }, new Set<PackID>());
      return new Map(gamePacks.filter(([id]) => ownedPackIds.has(id)));
    },
    load(gametype: Gametype, ownedPacks?: Map<PackID, object>) {
      const game = (config.games || []).find((game) => game.gametype === gametype);
      if (!game || !game.logic) {
        throw new Error(ErrorMessage.GameNotFound);
      }
      if (ownedPacks && !ownedPacks.has(game.packs.base.id)) {
        throw new Error(ErrorMessage.BasePackUnowned);
      }
      return game.logic;
    }
  };

  const getResourceLoaderForGametype = (gametype: Gametype) => {
    if (!config.games) {
      throw new Error();
    }
    const game = config.games.find((game) => game.gametype === gametype);
    if (!game) {
      throw new Error(ErrorMessage.GameNotFound);
    }
    const resources = game.resources || {};
    return {
      async load(uri: string, type?: XMLHttpRequestResponseType): Promise<any> {
        const data = resources[uri];
        switch (type) {
          case "arraybuffer": {
            const encoder = new TextEncoder();
            return encoder.encode(data).buffer;
          }
          case "blob": return new Blob([data]);
          case "document": {
            const parser = new DOMParser();
            return parser.parseFromString(data, "text/html");
          }
          case "json": return JSON.parse(data);
        }
        return data;
      },
    } as ResourceLoader;
  };

  const hostApp: HostApp = {
    async allGames(): Promise<ReadonlySet<GameDefinition>> {
      return new Set(config.games);
    },
    async ownedPacks() {
      let iterable: Iterable<PackID> | undefined;
      if (config.ownedPacks) {
        iterable = config.ownedPacks[config.host];
      }
      return new Set(iterable);
    },
    async purchasePack(id: PackID): Promise<boolean> {
      if (config.testPurchaseError) {
        throw new Error("Unspecified purchase error");
      }
      config.ownedPacks = config.ownedPacks || {};
      config.ownedPacks[config.host] = config.ownedPacks[config.host] || [];
      if (config.games) {
        const gameByExtra = config.games.find((gameDef) => gameDef.packs.extra.find((pack) => pack.id === id));
        if (gameByExtra) {
          if (!config.ownedPacks[config.host].includes(gameByExtra.packs.base.id)) {
            throw new Error(ErrorMessage.BasePackUnowned);
          }
        } else {
          const gameByBase = config.games.find((gameDef) => gameDef.packs.base.id === id);
          if (!gameByBase) {
            throw new Error(ErrorMessage.PackNotFound);
          }
        }
      }
      config.ownedPacks[config.host].push(id);
      return true;
    },
    async startRoom() {
      const room = new MockRoom(gameLoader, getResourceLoaderForGametype, config.roomUserLimit);
      rooms.set(room.lobbyCode, room);
      return room;
    },
  };

  const playerApps = new Map<UserID, MockPlayerApp>();

  for (const user of config.users) {
    const userObject = new MockUser(user);
    playerApps.set(user.id, new MockPlayerApp(userObject, rooms));
  }

  return {
    hostApp,
    playerAppForId(userId: UserID) {
      const playerApp = playerApps.get(userId);
      if (!playerApp) {
        throw new Error("User with that id does not exist");
      }
      return playerApp;
    },
  };
}

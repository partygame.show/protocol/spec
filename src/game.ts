import { PackID } from "./common/game";
import { MessagePayload } from "./common/room";
import { User, UserID } from "./common/user";
import { HostPlayer } from "./host";
import { PlayerMessage } from "./player";

export interface ResourceLoader {
  load(uri: string, type?: "" | "text"): Promise<string>;
  load(uri: string, type: "arrayBuffer"): Promise<ArrayBuffer>;
  load(uri: string, type: "blob"): Promise<Blob>;
  load(uri: string, type: "document"): Promise<Document>;
  load(uri: string, type: "json"): Promise<object>;
}

export interface HostGameResult {
  readonly scores: ReadonlyMap<UserID, number>;
}

export interface HostGameSession {
  readonly result: Promise<HostGameResult>;
  run(ctx: unknown): Promise<void>;
}

export interface HostGameLogic<PLAYER_STATE extends {} = object> extends HostGameSession {
  prepare(players: ReadonlyMap<UserID, HostPlayer<PLAYER_STATE>>, content: ReadonlyMap<PackID, object>, loader: ResourceLoader): Promise<void>;
  onPlayerExit(id: UserID): Promise<void>;
}

export interface PlayerGameSession {
  attach(ctx: unknown): Promise<void>;
}

export interface PlayerGameLogic<PLAYER_STATE extends {} = object, REQUEST extends MessagePayload = MessagePayload, RESPONSE extends MessagePayload = MessagePayload> extends PlayerGameSession {
  prepare(user: User, loader: ResourceLoader, end: Promise<void>): Promise<void>;
  onStateChange(state: PLAYER_STATE | null): Promise<void>;
  onMessage(message: PlayerMessage<REQUEST, RESPONSE>): Promise<void>;
  onClear(): Promise<void>;
}
